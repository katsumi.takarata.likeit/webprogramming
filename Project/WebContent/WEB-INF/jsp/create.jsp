<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>create</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/create.css" rel="stylesheet">
</head>
<body>
	<!-- ナビゲーションバー -->
	<header>

		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="#">ユーザー管理</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarText" aria-controls="navbarText"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav mr-auto">
				</ul>
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="#">${userInfo.name}
							さん</a></li>
					<li class="nav-item"><a class="btn btn-primary" href="Logout">ログアウト</a></li>
				</ul>
			</div>
		</nav>
	</header>

	<c:if test="${errMsg0 != null}">
		<div class="alert alert-danger" role="alert">${errMsg0}</div>
	</c:if>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<c:if test="${errMsg1 != null}">
		<div class="alert alert-danger" role="alert">${errMsg1}</div>
	</c:if>

	<h1 class="text-center">新規登録</h1>
	<div class="container">



		<form action="Create" method="post">
			<div class="form-group row">
				<label for="id" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<input type="text" name="loginId" class="form-control" id="id"
						placeholder="ID">
				</div>
			</div>
			<div class="form-group row">
				<label for="password" class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-10">
					<input type="password" name="password" class="form-control"
						id="password" placeholder="password">
				</div>
			</div>
			<div class="form-group row">
				<label for="repassword" class="col-sm-2 col-form-label">パスワード（確認）</label>
				<div class="col-sm-10">
					<input type="password" name="repassword" class="form-control"
						id="repassword" placeholder="password">
				</div>
			</div>
			<div class="form-group row">
				<label for="name" class="col-sm-2 col-form-label">ユーザー名</label>
				<div class="col-sm-10">
					<input type="text" name="name" class="form-control" id="name"
						placeholder="name">
				</div>
			</div>
			<div class="form-group row">
				<label for="birth" class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-10">
					<input type="date" name="birth" class="form-control" id="birth">
				</div>
			</div>
			<div class="submitButton">
				<div class="form-group row">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-primary btn-lg">登録</button>
					</div>
				</div>
			</div>

		</form>

	</div>

	<a href="List">戻る</a>



</body>
</html>