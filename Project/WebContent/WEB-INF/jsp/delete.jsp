<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>delete</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/create.css" rel="stylesheet">
</head>
<body>
	<!-- ナビゲーションバー -->
	<header>

		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="#">ユーザー管理</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarText" aria-controls="navbarText"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav mr-auto">
				</ul>
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="#">${userInfo.name}</a>
					</li>
					<li class="nav-item"><a class="btn btn-primary" href="Logout">ログアウト</a></li>
				</ul>
			</div>
		</nav>
	</header>

	<h1 class="text-center">ユーザー情報削除</h1>

	<div class="container">
		<div class="delete-area">
			<p>ID${detail.id}を消去しますか？</p>
			<div class="row">
				<div class="col-sm-6">
					<a href="List" class="btn btn-light">いいえ</a>
				</div>
				<div class="col-sm-6">
					<form action="Delete" method="post">
						<input type="hidden" name="id" value="${detail.id}">
						<button type="submit" class="btn btn-primary">はい</button>
					</form>
				</div>
			</div>
		</div>
	</div>

</body>
</html>