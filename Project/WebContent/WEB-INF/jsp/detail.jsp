<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>detail</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/create.css" rel="stylesheet">
</head>
<body>
	<!-- ナビゲーションバー -->
	<header>

		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="#">ユーザー管理</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarText" aria-controls="navbarText"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav mr-auto">
				</ul>
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="#">${userInfo.name} さん</a>
					</li>
					<li class="nav-item"><a class="btn btn-primary"
						href="Logout">ログアウト</a></li>
				</ul>
			</div>
		</nav>
	</header>


	<h1 class="text-center">ユーザー情報詳細参照</h1>
	<div class="container">

			<div class="form-group row">
				<label for="ID" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<p class="form-control-plaintext">${detail.loginId}</p>
				</div>
			</div>
			<div class="form-group row">
				<label for="name" class="col-sm-2 col-form-label">ユーザー名</label>
				<div class="col-sm-10">
					<p class="form-control-plaintext">${detail.name}</p>
				</div>
			</div>
			<div class="form-group row">
				<label for="birth" class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-10">
					<input type="text" readonly class="form-control-plaintext"
						id="birth" value="${detail.birthDate}">
				</div>
			</div>
			<div class="form-group row">
				<label for="createdate" class="col-sm-2 col-form-label">登録日時</label>
				<div class="col-sm-10">
					<input type="text" readonly class="form-control-plaintext"
						id="createdated" value="${detail.createDate}">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputPassword" class="col-sm-2 col-form-label">更新日時</label>
				<div class="col-sm-10">
					<input type="text" readonly class="form-control-plaintext"
						id="inputPassword" value="${detail.updateDate}5">
				</div>
			</div>


		<a href="List">戻る</a>
	</div>
</body>
</html>