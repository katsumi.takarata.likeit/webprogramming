<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>List</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <!-- オリジナルCSS読み込み -->
  <link href="css/list.css" rel="stylesheet">
</head>
<body>
  <header>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">ユーザー管理</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav mr-auto">
      </ul>
      <ul class="navbar-nav">
        <li class="nav-item">
        <a class="nav-link" href="#">${userInfo.name} さん</a>
        </li>
        <li class="nav-item">
        <a class="btn btn-primary" href="Logout">ログアウト</a>
        </li>
      </ul>
    </div>
  </nav>
  </header>

<div class="container">
   <!-- 新規登録ボタン -->
    <div class="create-button-area">
      <a class="btn btn-outline-primary btn-lg" href="Create">新規登録</a>
    </div>







    <!-- 検索ボックス -->
    <form action="List" method="post">
      <div class="form-group row">
            <label for="exampleInputEmail1" class="col-sm-2 col-form-label">ログインID</label>
            <div class="col-sm-10">
            <input type="text" name="loginId" class="form-control" id="exampleInputEmail1" placeholder="ID">
            </div>
      </div>
      <div class="form-group row">
            <label for="exampleInputPassword1" class="col-sm-2 col-form-label">ユーザー名</label>
            <div class="col-sm-10">
              <input type="text" name="name" class="form-control" id="exampleInputPassword1" placeholder="name">
            </div>
      </div>
      <div class="form-group row">
              <label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>

              <div class="row col-sm-10">
                <div class="col-sm-5">
                  <input type="date" name="date-start" id="date-start" class="form-control" />
                </div>

                <div class="col-sm-1 text-center">
                  ~
                </div>
                <div class="col-sm-5">
                  <input type="date" name="date-end" id="date-end" class="form-control" />
                </div>
              </div>
            </div>
      <div class="text-right">
      <button type="submit" class="btn btn-primary">検索</button>
      </div>
    </form>

<!-- 検索結果 -->
<table class="table">
  <thead>
    <tr>
      <th scope="col-sm">ログインID</th>
      <th scope="col-sm">ユーザー名</th>
      <th scope="col-sm">生年月日</th>
      <th scope="col-sm">情報</th>
    </tr>
  </thead>
  <tbody>
  	<c:forEach var="u" items="${list}">
    <tr>
      <td>${u.loginId}</td>
      <td>${u.name}</td>
      <td>${u.birthDate}</td>
      <td> <a class="btn btn-primary" href="Detail?id=${u.id}">詳細</a>



      <c:if test="${userInfo.loginId == u.loginId}">
       <a class="btn btn-success" href="Update?id=${u.id}">更新</a>
       </c:if>

       <c:if test="${userInfo.loginId == 'admin'}">
       <a class="btn btn-success" href="Update?id=${u.id}">更新</a>
       <a class="btn btn-danger" href="Delete?id=${u.id}">削除</a>
       </c:if>
     </td>
    </tr>
    </c:forEach>

</tbody>
</table>
<a href="Login">戻る</a>
</div>
</body>
</html>
