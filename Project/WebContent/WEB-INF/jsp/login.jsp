<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
 <link href="css/login.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<p style= "text-align:center">
	ログイン画面
	</p>
<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<form action="Login" method="post">
	<div class="login-area">
		<div class="row">
	        <div class="col-sm-12">
	            ログインID<input type="text" name ="loginId" style="width:300px;" class="form-control">
	             </div>
	        </div>
	       <div class="row">
	        	<div class="col-sm-12">
	            	パスワード<input type="password" name ="password" style="width:300px;" class="form-control">
	             </div>
	        </div>
	        <div class="col-sm-6">
	             <button type="submit" style="width:100px;" class="btn btn-primary">ログイン</button>
	        </div>
      </div>
	</form>


</body>
</html>