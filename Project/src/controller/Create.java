package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class Create
 */
@WebServlet("/Create")
public class Create extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Create() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
				String loginId = request.getParameter("loginId");
				String password = request.getParameter("password");
				String repassword = request.getParameter("repassword");
				String name = request.getParameter("name");
				String birthDate = request.getParameter("birth");

				UserDao userDao = new UserDao();

				//すでに登録されているログインIDが入力された場合
				if (userDao.findLoginId(loginId)) {
					// リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg0", "入力された内容は正しくありません");
					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
					dispatcher.forward(request,response);
					return;
				}

				// passwordとrepasswordの値が異なる時
				if (!password.equals(repassword)) {
					// リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg", "入力された内容は正しくありません");
					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
					dispatcher.forward(request,response);
					return;
				}

				//入力項目に一つでも未入力
				if (loginId.equals("")||password.equals("")||repassword.equals("")||name.equals("")||birthDate.equals("")) {
					// リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg1", "入力された内容は正しくありません");
					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
					dispatcher.forward(request,response);
					return;
				}




		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userdao = new UserDao();
		userdao.create(loginId,password,name,birthDate);

		response.sendRedirect("List");
	}

}
