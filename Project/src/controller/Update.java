package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Update
 */
@WebServlet("/Update")
public class Update extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Update() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		// 確認用：idをコンソールに出力
		System.out.println(id);

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
		UserDao userDao = new UserDao();
		User detail = userDao.detail(id);

		request.setAttribute("detail", detail);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
	};

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		// リクエストパラメータの入力項目を取得
		String  id = request.getParameter("id");
		String password = request.getParameter("password");
		String repassword = request.getParameter("repassword");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birth");

		//パスワードとパスワード(確認)がどちらも空欄の場合はパスワードは更新せず、パスワード以外の項目を更新する
		if (password.equals("")&&repassword.equals("")) {
			UserDao userDao = new UserDao();
			userDao.updateNotPass(id, name, birthDate);
			response.sendRedirect("List");
			return;
		}

		// passwordとrepasswordの値が異なる時
		if (!password.equals(repassword)) {
			UserDao userDao = new UserDao();
			User detail = userDao.detail(id);

			request.setAttribute("detail", detail);
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request,response);
			return;
		}

		//入力項目に一つでも未入力
		if (name.equals("")||birthDate.equals("")) {
			UserDao userDao = new UserDao();
			User detail = userDao.detail(id);

			request.setAttribute("detail", detail);
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg1", "入力された内容は正しくありません");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request,response);
			return;
		}


		UserDao userDao = new UserDao();
		userDao.update(id,password,name,birthDate);

		response.sendRedirect("List");
	}

}
