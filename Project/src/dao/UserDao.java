package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			//暗号化
			String pass = md5(password);

			// 確認済みのSQL
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";
						// SELECTを実行し、結果表を取得
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, pass);
			ResultSet rs = ps.executeQuery();

			// ログイン失敗時の処理 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理 必要なデータのみインスタンスのフィールドに追加
			String loginData = rs.getString("login_id");
			String nameDate = rs.getString("name");
			return new User(loginData, nameDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

	//        全てのユーザ情報を取得する
	public ArrayList<User> findAll() {
		Connection con = null;
		ArrayList<User> list = new ArrayList<User>();

		try {
			// データベースへ接続
			con = DBManager.getConnection();
			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id != 1";
			// SELECTを実行し、結果表を取得
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");

				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
				list.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e2) {
					e2.printStackTrace();
					return null;
				}
			}

		}
		return list;
	}

	public ArrayList<User> search(String loginIdP, String nameP, String dateStartP, String dateEndP) {
		Connection con = null;
		ArrayList<User> list = new ArrayList<User>();

		try {
			// データベースへ接続
			con = DBManager.getConnection();
			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id != 1";

			if(!loginIdP.equals("")) {
				sql += " AND login_id = '" + loginIdP + "'";
			}


			if(!nameP.equals("")) {
				sql += " AND name LIKE  '%" + nameP + "%'";
			}

			if(!dateStartP.equals("")) {
				sql += " AND birth_date >= '" + dateStartP + "'";
			}

			if(!dateEndP.equals("")) {
				sql += " AND birth_date <= '" + dateEndP + "'";
			}

			System.out.println(sql);

			// SELECTを実行し、結果表を取得
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");

				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
				list.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e2) {
					e2.printStackTrace();
					return null;
				}
			}

		}
		return list;
	}


	//新規登録
	public void create(String loginId, String password, String name, String birthDate) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// TODO ここに処理を書いていく
			//暗号化
			String pass = md5(password);
			// 確認済みのSQL
			String sql = "INSERT INTO user ( login_id, password, name, birth_date, create_date, update_date) VALUES( ?, ?, ?, ?, NOW(), NOW())";

			// INSERTを実行
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, pass);
			ps.setString(3, name);
			ps.setString(4, birthDate);

			ps.executeUpdate();



		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}
	//ユーザー詳細
	public User detail(String id) {
		Connection con = null;
		User detail = null;

		try {
			// データベースへ接続
			con = DBManager.getConnection();
			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id = ?";
			// SELECTを実行し、結果表を取得
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定しインスタンスに追加
			while (rs.next()) {
				int ID = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");

				detail = new User(ID, loginId, name, birthDate, password, createDate, updateDate);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e2) {
					e2.printStackTrace();
					return null;
				}
			}

		}
		return detail;

	}

	public void update(String id, String password, String name, String birthDate) {
		Connection con = null;

		try {
			// データベースへ接続
			con = DBManager.getConnection();

			//暗号化
			String pass = md5(password);
			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "UPDATE user SET password = ?, name = ?, birth_date = ?, update_date = NOW()  WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, pass);
			ps.setString(2, name);
			ps.setString(3, birthDate);
			ps.setString(4, id);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
			}

		}

	}
	public void delete(String id) {
		Connection con = null;

		try {
			// データベースへ接続
			con = DBManager.getConnection();
			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "DELETE FROM user WHERE id = ?";
			// SELECTを実行
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, id);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
			}

		}

	}

	//暗号化のメソッド
		private static String md5(String password) {
			//ハッシュを生成したい元の文字列
					String source = password;
					//ハッシュ生成前にバイト配列に置き換える際のCharset
					Charset charset = StandardCharsets.UTF_8;
					//ハッシュアルゴリズム
					String algorithm = "MD5";

					//ハッシュ生成処理
					byte[] bytes = null;
					try {
						bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
					} catch (NoSuchAlgorithmException e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
					}
					String result = DatatypeConverter.printHexBinary(bytes);
					//標準出力 passwordがresultに変わりました
					System.out.println(result);
					return result;
		}

			////すでに登録されているログインIDが入力された場合のメソッド
		public boolean findLoginId(String loginId) {

			Connection conn = null;
			try {
				// データベースへ接続
				conn = DBManager.getConnection();



				// 確認済みのSQL
				String sql = "SELECT * FROM user WHERE login_id = ?";
							// SELECTを実行し、結果表を取得
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, loginId);
				ResultSet rs = ps.executeQuery();

				// ログイン失敗時の処理 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う

				if (!rs.next()) {
					return false;
				}

				// ログイン成功時の処理 必要なデータのみインスタンスのフィールドに追加
				return true;

			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return false;
					}
				}
			}
		}
		public void updateNotPass(String id, String name, String birthDate) {
			Connection con = null;

			try {
				// データベースへ接続
				con = DBManager.getConnection();


				// TODO: 未実装：管理者以外を取得するようSQLを変更する
				String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = NOW()  WHERE id = ?";

				// SELECTを実行し、結果表を取得
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1,name );
				ps.setString(2,birthDate );
				ps.setString(3,id );
				ps.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
				}

			}

		}
}
