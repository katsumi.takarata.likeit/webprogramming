use usermanagementdb;

create table user (
	id serial,
	login_id varchar(255) unique not null,
	name varchar(255) not null,
	birth_date date not null,
	password varchar(255) not null,
	create_date datetime not null,
	update_date datetime not null
	);

insert into user(
login_id,
name,
birth_date,
password,
create_date,
update_date
)
values(
'admin',
'管理者',
'2019-03-03',
'1234',
now(),
now()
);